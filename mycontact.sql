create database mycontact

CREATE TABLE contact (
 id text(11) NOT NULL AUTO_INCREMENT,
 name Text (30) NOT NULL,
 email varchar(50) NULL,
 phone varchar(20) NULL,
 PRIMARY KEY (id)
);
 ENGINE=InnoDB DEFAULT CHARSET=utf8